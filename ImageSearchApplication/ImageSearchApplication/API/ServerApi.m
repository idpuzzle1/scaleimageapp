//
//  ServerApi.m
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import "ServerApi.h"
#import <AFNetworking.h>
#import "ImageModel.h"

@interface ServerApi()
{
    NSString *hostURL;
    NSString *serverKey;
    NSString *cxKey;
    //AFHTTPRequestOperationManager *manager;
    AFHTTPSessionManager *manager;
}

@end

@implementation ServerApi

static ServerApi *sharedInstance = nil;

#pragma mark - Singleton methods
+ (ServerApi *)sharedInstance
{
    @synchronized(self)
    {
        if (sharedInstance == nil) {
            sharedInstance = [[ServerApi alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    hostURL = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ServerHost"];
    serverKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ServerKey"];
    cxKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CxKey"];


    manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return self;
}

#pragma mark - Server Calls methods
//https://www.googleapis.com/customsearch/v1?key=SERVER_KEY&cx=CUSTOM_SEARCH_ID&q=flower&searchType=image&fileType=jpg&imgSize=xlarge&alt=json

-(void) getImagesForRequest:(NSString *)requestString withOffset:(int)offset onSuccess:(imagesBlock)successBlock onError:(errorBlock)errorBlock {
    
    
    
    NSString *requestURL = [NSString stringWithFormat:@"%@/customsearch/v1",hostURL];
    
    NSDictionary *params = @{
                             @"key"         : serverKey,
                             @"cx"          : cxKey,
                             @"q"           : requestString,
                             @"start"       : [NSNumber numberWithInt:offset+1],
                             @"searchType"  : @"image",
                             @"filter"      : @"0",
                             @"fileType"    : @"jpg",
                             @"imgSize"     : @"xLarge",
                             @"alt"         : @"json"
                             };
    
    [manager GET:requestURL parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSArray *images = [ImageModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"items"] error:nil];
        if (successBlock) {
            successBlock(images);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        //Reading error body
        NSData* errResponse = (NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSMutableDictionary* jsonDict = [[NSJSONSerialization JSONObjectWithData:errResponse
                                                                 options:kNilOptions
                                                                   error:&error] mutableCopy];
        NSString *errorMessage = jsonDict[@"error"][@"message"];
        if (errorBlock) {
        if (errorMessage) {
            [jsonDict setValue:errorMessage forKey:NSLocalizedDescriptionKey];
            errorBlock([[NSError alloc] initWithDomain:error.domain code:error.code userInfo:jsonDict]);
        } else {
            errorBlock(error);
        }
        }
    }];
}

-(void) getImagesForRequest:(NSString *)requestString onSuccess:(imagesBlock)successBlock onError:(errorBlock)errorBlock  {
    
    [self getImagesForRequest:requestString withOffset:0 onSuccess:successBlock onError:errorBlock];
}

@end
