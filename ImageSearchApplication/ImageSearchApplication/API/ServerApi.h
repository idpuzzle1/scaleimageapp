//
//  ServerApi.h
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import <Foundation/Foundation.h>

typedef void (^imagesBlock)(NSArray *);
typedef void(^errorBlock)(NSError *);

@interface ServerApi : NSObject

+ (ServerApi *)sharedInstance;

-(void) getImagesForRequest:(NSString *)requestString withOffset:(int)offset onSuccess:(imagesBlock)successBlock onError:(errorBlock)errorBlock;
-(void) getImagesForRequest:(NSString *)requestString onSuccess:(imagesBlock)successBlock onError:(errorBlock)errorBlock;
@end
