//
//  ImageModel.m
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import "ImageModel.h"

@implementation ImageModel

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithModelToJSONDictionary:@{
                                            @"title": @"title",
                                            @"link": @"link",
                                            @"properties": @"image"
                                            }];
}

@end
