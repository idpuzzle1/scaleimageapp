//
//  ImageProperties.h
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import <JSONModel/JSONModel.h>

@interface ImagePropertiesModel : JSONModel

@property (nonatomic) float width;
@property (nonatomic) float height;

@end
