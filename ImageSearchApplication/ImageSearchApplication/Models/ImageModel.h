//
//  ImageModel.h
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import <JSONModel/JSONModel.h>
#import "ImagePropertiesModel.h"

@interface ImageModel : JSONModel

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *link;
@property (nonatomic) ImagePropertiesModel *properties;

@end

