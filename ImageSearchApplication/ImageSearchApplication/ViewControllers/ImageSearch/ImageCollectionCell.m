//
//  ImageCollectionCell.m
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import "ImageCollectionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ImageCollectionCell

- (void)configWithUrl:(NSString *)url {
    NSURL *imageUrl = [NSURL URLWithString:url];
    [self.imageView sd_setImageWithURL:imageUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
}

- (void)prepareForReuse {
    [self.imageView setImage:nil];
}

@end
