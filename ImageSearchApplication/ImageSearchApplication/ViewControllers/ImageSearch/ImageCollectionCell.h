//
//  ImageCollectionCell.h
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import <UIKit/UIKit.h>

@interface ImageCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void)configWithUrl:(NSString *)url;

@end
