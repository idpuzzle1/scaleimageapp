//
//  ViewController.m
//  ImageSearchApplication
//
//  Created by Антон Уханкин on 25.01.17.
//
//

#import "ImageSearchVC.h"
#import "ImageCollectionCell.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import <AFNetworking.h>

#import "ServerApi.h"
#import "ImageModel.h"
#import <Haneke.h>

#define ContentSizeChangedObserverKey @"contentSize"
#define ImageCollectionCellReuseIdentifier @"ImageCollectionCell"

@interface ImageSearchVC () <UISearchBarDelegate,CHTCollectionViewDelegateWaterfallLayout,UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSString *requestText;
    BOOL allLoaded;
    BOOL isLoading;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISlider *scaleSlider;
@property (weak, nonatomic) IBOutlet UICollectionView *imagesCollectionView;
@property (strong, nonatomic) CHTCollectionViewWaterfallLayout *layout;

@property NSMutableArray *images;

@end

@implementation ImageSearchVC

@synthesize imagesCollectionView,layout,scaleSlider,searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    
    requestText = searchBar.text;
    [self reloadImages];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary  *)change context:(void *)context
{
    if ([keyPath isEqualToString:ContentSizeChangedObserverKey]) {
        if (imagesCollectionView.contentSize.height < imagesCollectionView.frame.size.height) {
            [self loadMoreImages];
        }
    }
}

- (void)dealloc
{
    [imagesCollectionView removeObserver:self forKeyPath:@"contentSize" context:NULL];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [imagesCollectionView performBatchUpdates:^{
            [imagesCollectionView setCollectionViewLayout:layout animated:true];
        } completion:nil];
    }];
}

- (IBAction)imageScaleChanged:(id)sender {
    [layout invalidateLayout];
    
}

- (void) initView {
    layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    
    [imagesCollectionView setCollectionViewLayout:layout animated:true];
    [imagesCollectionView addObserver:self forKeyPath:ContentSizeChangedObserverKey options:NSKeyValueObservingOptionOld context:NULL];
        
    scaleSlider.minimumValue = 0.1f;
    scaleSlider.maximumValue = 1.0f;
    scaleSlider.value = (scaleSlider.minimumValue+scaleSlider.maximumValue)/2.5f;
    
}

- (void)reloadImages {
    allLoaded = false;
    isLoading = false;
    self.images = [NSMutableArray new];
    [self.imagesCollectionView reloadData];
    [self loadMoreImages];
}

- (void)loadMoreImages {
    if (isLoading || allLoaded) { return; }
    isLoading = true;
    [[ServerApi sharedInstance] getImagesForRequest:requestText withOffset:(int)self.images.count onSuccess:^(NSArray *images) {
        isLoading = false;
        [self.images addObjectsFromArray:images];
        [self.imagesCollectionView reloadData];
    } onError:^(NSError *error) {
        isLoading = false;
        allLoaded = true;
        [self showErrorMessage:error];
    }];
}

- (void)showErrorMessage:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:error.localizedDescription
                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - SearchBar Delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    requestText = searchText;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self reloadImages];
    [self.view endEditing:true];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageModel *image = [self.images objectAtIndex:indexPath.row];
    CGSize cellSize = CGSizeZero;
    float aspectRatio = image.properties.height/image.properties.width;
    cellSize.width = scaleSlider.value * collectionView.contentSize.width;
    cellSize.height = cellSize.width * aspectRatio;
    
    return cellSize;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout columnCountForSection:(NSInteger)section {
    
    float cellWidth = scaleSlider.value * collectionView.contentSize.width;
    return (collectionView.contentSize.width - layout.sectionInset.left - layout.sectionInset.right) / cellWidth;
    
}

#pragma mark - CollectionView DataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.images.count ?: 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [collectionView dequeueReusableCellWithReuseIdentifier:ImageCollectionCellReuseIdentifier forIndexPath:indexPath];
    
}

#pragma mark - CollectionView Delegate methods

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionCell *imageCell = (ImageCollectionCell *)cell;
    if (imageCell) {
        ImageModel *image = [self.images objectAtIndex:indexPath.row];
        imageCell.backgroundColor = [UIColor grayColor];
        [imageCell configWithUrl:image.link];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - ScrollView Delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    CGFloat deltaOffset = maximumOffset - currentOffset*2;
    
    if (deltaOffset <= 0) {
        [self loadMoreImages];
    }

}




@end
